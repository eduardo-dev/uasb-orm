<?php

namespace App\Models\Bears;

use Illuminate\Database\Eloquent\Model;

class Bear extends Model
{
    protected $table = "bears";

    protected $danger_limit = 8;

    protected $fillable = [
        'name',
        "danger_level"
    ];

    protected $casts =  [
        "danger_level"  => "integer",
        'updated_at'    => "date",
        'created_at'    => "date"
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
        "id",
        "trees",

    ];

    protected $appends = [
        "is_bad",
        "total_trees",
        "grouped_trees"
    ];

    public function getTotalTreesAttribute()
    {
        return $this->trees->count();
    }

    public function getGroupedTreesAttribute()
    {
        return $this->trees->groupBy("type")->map(function($trees){
            return $trees->count();
        });
    }


    public function trees()
    {
        return $this->belongsToMany(Tree::class);
    }


    public function getIsBadAttribute()
    {
        return (boolean) ($this->danger_level >= $this->danger_limit);
    }


    public function scopeBads($query)
    {
        return $query-> where("danger_level",">=",$this->danger_limit);
    }

    public function scopeLikeTreesByType($query,$tree_type)
    {
        return $query->with(["trees" => function($q) use ($tree_type){
            return $q->byType($tree_type);
        }])
            ->whereHas("trees",function($q)use ($tree_type){
                return $q->byType($tree_type);
            });
    }



}
