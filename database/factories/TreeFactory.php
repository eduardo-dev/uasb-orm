<?php

use Faker\Generator as Faker;
use App\Models\Bears\Tree;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Tree::class, function (Faker $faker) {

    $types = [
        "abeto",
        "pino",
        "roble"
    ];

    return [
        'type'          => $types [array_rand($types,1)] ,
        'age'           => rand(20,150),
    ];
});
