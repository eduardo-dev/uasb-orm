<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ForestsTableSeeder::class);
        $this->call(TreesTableSeeder::class);
        $this->call(BearsTableSeeder::class);
    }
}
