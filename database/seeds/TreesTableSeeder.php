<?php

use Illuminate\Database\Seeder;
use App\Models\Bears\Tree;

class TreesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Tree::class,1000)->create();
    }
}
